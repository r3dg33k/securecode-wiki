---
title: Farid Luhar
image: 
    url: ./farid_image.jpg
    alt: Farid_image    
website: # Your Website Link
socials:
    linkedin: https://www.linkedin.com/in/farid-luhar/
    twitter: https://twitter.com/faridpy
    github: https://github.com/farid007/
draft: false # Change it to false if you want it published.
# Do not change the below values.
type: contributors
layout: single
# Note: Always use https:// whenever putting up links. For e.g., https://payatu.com
# All the fields above are optional
---

Farid is an experienced Security Consultant in Payatu. Experienced in Web Application, Mobile Application, Think Client Application penetration testing & Infrasture penetration testing and source code review. He holds OSCP & OSWE certificates. He is also an expert in Python, PHP, JavaScript and Java languages.   
